# Vue Form UI Components

## Install
```
npm install bolt-inputs -s
```

### Usage
```
import boltInputs from 'bolt-inputs'
Vue.use(boltInputs)

Example

bolt input has four types: text, email, password and tel
<bolt-input type="text" id="textField" label="Text Field" :required="true"/>
<bolt-input type="email" />
<bolt-input type="password" />
<bolt-input type="tel" />
```
