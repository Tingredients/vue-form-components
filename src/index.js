/**
 * External library
 */
import Vuelidate from 'vuelidate';
const VueInputMask = require('vue-inputmask').default;
import boltInput from '@/inputs/inputs'

/**
 *Directives
 */
import clearinput from '@/directives/clear-input';

const components = [
    boltInput
];

const boltUI = {
    install: function (Vue) {
        components.map(component => {
            Vue.component(component.name, component);
        });

        Vue.use(VueInputMask);
        Vue.use(Vuelidate);

        Vue.directive('clearinput', clearinput);
    }
}

// Auto-install when vue is found (eg. in browser via <script> tag)
if (typeof window !== 'undefined' && window.Vue) {
    boltUI.install(window.Vue);
}
//
// Vue.use(boltInputs)

export default boltUI