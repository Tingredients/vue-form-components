// Initialize the annoying-background directive.
export const tabIndex = {
    bind: function (el, binding, vnode) {
        let index = parseInt(binding.arg);
        let maxlength = parseInt(binding.value.maxlength);
        let input;
        let id;

        el.addEventListener("keyup", function (e) {
            if (e.code === 'Tab') {
                if (index === 1) {
                    el.focus();
                } else if (index === 2) {
                    el.focus();
                }
            } else {
                if (e.target.value.length === maxlength) {
                    if (index !== 2) {
                        if (index === 0) {
                            document.getElementById(binding.value.next).focus();
                        } else if (index === 1) {
                            document.getElementById(binding.value.next).focus();
                        }
                    }
                }
            }
        });

        el.addEventListener("blur", function (e) {
            id = e.target.id;
            input = document.getElementById(id);
            if (input.value.length === 1 && maxlength === 2) {
                vnode.context.form.birth[id] = 0 + input.value;
            }
        }, true);
    },
    inserted: function (el, binding, vnode) {
    },
    update: function (el, binding, vnode) {

    }
};