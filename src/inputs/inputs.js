import { required, numeric, minLength } from 'vuelidate/lib/validators';
import base from './mixins/base';

export default {
    name: 'bolt-input',
    mixins: [base],
    data() {
        return {
            defaultMaskSetting: {
                regex: this.maskRegex,
                autoUnmask: true,
                showMaskOnHover: false,
                showMaskOnFocus: false
            },
            confirmpassword: ''
        }
    },
    props: {
        mask: {
            type: String,
            default: ''
        },
        maskPlaceholder: {
            type: String,
            default: ''
        },
        maskRegex: {
            type: String
        },
        confirm: {
            type: Boolean,
            default: false
        }
    },
    computed: {
        maskSetting() {
            if (this.mask !== '') {
                this.defaultMaskSetting.mask = this.mask;
                this.defaultMaskSetting.placeholder = this.maskPlaceholder === '' ? ' ' : this.maskPlaceholder;
                return this.defaultMaskSetting;
            } else {
                return this.defaultMaskSetting;
            }
        }
    },
    validations() {
        if (this.required) {
            switch(this.type) {
                case 'email':
                    return this.validation().emails
                    break
                case 'password':
                    return this.validation().pwd(this.minlength)
                    break
                case 'tel':
                    return this.validation().tel(this.minlength)
                    break
                default:
                    if (this.minlength !== 0) {
                        return {
                            value: {
                                required,
                                minLength: minLength(this.minlength)
                            }
                        }
                    } else {
                        return {
                            value: {
                                required
                            }
                        }
                    }
                    break;
            }
        } else {
            if (this.minlength !== 0) {
                return {
                    value: {
                        minLength: minLength(this.minlength)
                    }
                }
            } else {
                if (this.type === 'tel') {
                    return {
                        value: {
                            numeric
                        }
                    }
                }
            }
        }
    },
    render(h) {
        let requiredAlertMessage, invalidAlertMessage, helperText, properties, confirmPwd, inputs, confirmPwdField;

        /**
         * Required Message
         */
        if (this.isRequired) {
            requiredAlertMessage = <span class="form-group__message validation_message">{ this.requiredAlertMessage }</span>
        }

        /**
         * Invalid Message
         */
        if (this.minlength !== 0 || this.isInvalid) {
            invalidAlertMessage = <span class="form-group__message validation_message">{ this.isInvalid }</span>
        }

        /**
         * Helper text
         */
        if (this.helperText) {
            helperText = <p class="help">{ this.helperText }</p>
        }

        /**
         * Input attributes
         */
        properties = {
            attrs: {
                id: this.id,
                type: this.type,
                placeholder: this.placeholder,
                maxlength: this.maxlength === undefined?undefined:this.maxlength,
                readonly: this.disable,
                disabled: this.disable,
            },
            props: {
                value: this.value
            },
            on: {
                input: e => {
                    this.updateValue(e.target.value)
                },
                focus: e => {
                    this.removeError(e)
                }
            },
            directives: [
                {
                    name: 'clearinput'
                },
                {
                    name: 'mask',
                    value: this.maskSetting
                }
            ]
        };
        inputs = h('input', properties);

        /**
         * Confirm password properties
         *
         */
        confirmPwd = {
            attrs: {
                id: 'confirmPwd',
                type: 'password',
                placeholder: 'Confirm Password'
            },
            props: {
                value: this.confirmpassword
            },
            on: {
                input: e => {
                    this.confirmpassword = e.target.value
                }
            },
            directives: [
                {
                    name: 'clearinput'
                }
            ]
        };
        if (this.confirm) {
            confirmPwdField = <div>
                                <label class="gfield_label" for="confirmPwd">Confirm Password*</label>
                                { h('input', confirmPwd) }
                             </div>
        } else {
            confirmPwdField = ''
        }


        if (this.show) {
            return (
                <li class="gfield col-xs-12" class={ this.showErrorClass()?'error':'' }>
                    <label class="gfield_label col-sm-3" for={this.id}>{ this.label }</label>
                    <div class="ginput_container">
                        { inputs }
                        { requiredAlertMessage }
                        { invalidAlertMessage }
                        { confirmPwdField }
                        { helperText }
                    </div>
                </li>
            )
        } else {
            return ''
        }
    }
}
