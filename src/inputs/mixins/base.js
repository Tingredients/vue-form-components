import { emails, pwd, tel } from './form-validation';

export default {
    props: {
        value: {
            type: [Object, Array, Number, String, Boolean]
        },
        id: {
            type: String
        },
        label: {
            type: String
        },
        helperText: {
            type: String,
            default: ''
        },
        required: {
            type: Boolean,
            default: false
        },
        maxlength: {
            type: Number,
            default: undefined
        },
        minlength: {
            type: Number,
            default: 0
        },
        formSubmission: {
            type: Boolean,
            default: false
        },
        type: {
            type: String
        },
        placeholder: {
            type: String
        },
        requiredAlertMessage: {
            type: String,
            default: 'The field is required'
        },
        show: {
            type: Boolean,
            default: true
        },
        disable: {
            type: Boolean,
            default: false
        },
        invalidAlertMessage: {
            type: String,
            default: null
        }
    },
    computed: {
        isRequired () {
            if (this.required) {
                return !this.$v.value.required && this.$v.value.$dirty || !this.$v.value.required && this.formSubmission;
            } else {
                return false;
            }
        },
        isInvalid() {
            if (this.required) {
                switch (this.type) {
                    case 'email':
                        if (!this.$v.value.email && this.$v.value.$dirty || !this.$v.value.email && this.formSubmission) {
                            return this.invalidAlertMessage || 'Invalid email address'
                        } else {
                            return false
                        }
                        break;
                    case 'password':
                        if (this.confirm) {
                            if (!this.$v.value.minLength && this.minlength !== 0 && this.formSubmission) {
                                return `${this.placeholder} must have at least ${this.minlength} characters.`;
                            } else {
                                if (!this.$v.confirmpassword.sameAsPassword && this.formSubmission) {
                                    return this.invalidAlertMessage || 'Passwords must be identical';
                                } else {
                                    return false;
                                }
                            }
                        } else {
                            if (!this.$v.value.minLength && this.minlength !== 0 && this.formSubmission) {
                                return `${this.placeholder} must have at least ${this.minlength} characters.`;
                            } else {
                                return false;
                            }
                        }
                        break;
                    case 'tel':
                        if (!this.$v.value.numeric) {
                            return this.invalidAlertMessage || 'This field must be in digits'
                        } else {
                            if (!this.$v.value.minLength && this.minlength !== 0) {
                                return `${this.placeholder} must have at least ${this.minlength} characters.`;
                            } else {
                                return false;
                            }
                        }
                        break;
                    default:
                        if (!this.$v.value.minLength && this.minlength !== 0 && this.$v.value.$dirty && this.formSubmission ) {
                            return `${this.placeholder} must have at least ${this.minlength} characters.`;
                        } else {
                            return false;
                        }
                        break
                }
            } else {
                return false;
            }
        }
    },
    methods: {
        updateValue (value) {
            if (this.required) {
                this.$v.value.$touch();
            }
            this.$emit('input', value);
        },
        showErrorClass () {
            if (this.required) {
                return this.$v.value.$error || !this.$v.value.required && this.formSubmission;
            } else {
                return false;
            }
        },
        removeError (event) {
            this.$emit('removeError', event);
        },
        validation () {
            return {
                emails,
                pwd,
                tel
            }
        }
    }
};