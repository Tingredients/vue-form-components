import moment from 'moment';

const isOverAge = (birth, component) => {

    let dateString = `${birth.year}-${birth.month}-${birth.day}`;

    let today = new Date();
    let birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age >= 18;
};

const isInvalid = (birth, component) => {

    if (birth.year.length === 4 && birth.month.length === 2 && birth.day.length === 2) {
        let dateString = `${birth.year}-${birth.month}-${birth.day}`;

        let dob = moment(dateString);
        return dob.isValid();
    } else {
        return false;
    }
};

export { isOverAge, isInvalid };