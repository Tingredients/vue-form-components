import { required, numeric, sameAs, minLength, email } from 'vuelidate/lib/validators';
// import { isOverAge, isInvalid } from './customvalidation';

const emails = {
    value: {
        required,
        email
    }
};

const pwd = (minlength) => {
    return {
        value: {
            required,
            minLength: minLength(minlength)
        },
        confirmpassword: {
            required,
            sameAsPassword: sameAs('value')
        }
    }
};

const tel = (minlength) => {
    return {
        value: {
            required,
            numeric,
            minLength: minLength(minlength)
        }
    }
};

export { emails, pwd, tel }