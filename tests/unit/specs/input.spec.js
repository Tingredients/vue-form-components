import Vue from 'vue';
import { mount, shallowMount } from '@vue/test-utils';
import Vuelidate from 'vuelidate';
import {required, numeric, minLength} from 'vuelidate/lib/validators';
import clearinput from '@/directives/clear-input';
import boltInput from '@/inputs/input';

const VueInputMask = require('vue-inputmask').default;

Vue.use(Vuelidate);
Vue.use(VueInputMask);

Vue.directive('clearinput', clearinput);

let setComponent = function (component, propsData) {
    const input = mount(component, { propsData: propsData });
    return input;
};

describe('input.vue', () => {
    it('should render correct id', () => {
        let wrapper = setComponent(boltInput, {id: 'bolt-input'});
        expect(wrapper.find('input').attributes().id).toBe('bolt-input');
    });

    it('should render correct label', () => {
        let wrapper = setComponent(boltInput, {label: 'Bolt Input'});
        expect(wrapper.find('.gfield_label').text()).toEqual('Bolt Input');
    });

    it('should render correct helper text', () => {
        let wrapper = setComponent(boltInput, {helperText: 'This is helper text'});
        expect(wrapper.find('.help').text()).toBe('This is helper text');
    });

    it('should render correct input type', () => {
        let wrapper = setComponent(boltInput, {
            type: 'text'
        });

        let wrapper1 = setComponent(boltInput, {
            type: 'email'
        });
        expect(wrapper1.find('input').attributes().type).toBe('email');
    });

    it('should render correct placeholder', () => {
        let wrapper = setComponent(boltInput, {
            placeholder: 'Type text here'
        });
        expect(wrapper.find('input').attributes().placeholder).toBe('Type text here');
    });

    it('Should be required', () => {

        /**
         * Text type
         * @type {Wrapper<Vue>}
         */
        let textfield = setComponent(boltInput, {
            type: 'text',
            value: '',
            placeholder: 'Text field',
            required: true,
            formSubmission: true,
            requiredAlertMessage: 'This field is required'
        });
        expect(textfield.vm.isRequired).toBe(true);
        expect(textfield.find('.validation_message').text()).toBe('This field is required');

        /**
         * Text type has min length when submission
         * @type {Wrapper<Vue>}
         */
        let textfield1 = setComponent(boltInput, {
            type: 'text',
            value: 'abcde',
            placeholder: 'Text field',
            required: true,
            formSubmission: true,
            minlength: 8,
            requiredAlertMessage: 'This field is required'
        });

        //if input $dirty & required
        expect(textfield1.vm.isRequired).toBe(false);
        expect(textfield1.find('.validation_message').text()).toBe('Text field must have at least 8 characters.');

        /**
         * Email type
         */
        let emailfield = setComponent(boltInput, {
            type: 'email',
            value: '',
            placeholder: 'Email field',
            required: true,
            formSubmission: true,
            requiredAlertMessage: 'This field is required',
            invalidAlertMessage: 'Invalid email address'
        });
        expect(emailfield.vm.isRequired).toBe(true);
        expect(emailfield.find('.validation_message').text()).toBe('This field is required');

        let emailfield1 = setComponent(boltInput, {
            type: 'email',
            value: 'abcde',
            placeholder: 'Email field',
            required: true,
            formSubmission: true,
            requiredAlertMessage: 'This field is required',
            invalidAlertMessage: 'Invalid email address'
        });
        expect(emailfield1.vm.isRequired).toBe(false);
        expect(emailfield1.find('.validation_message').text()).toBe('Invalid email address');

        /**
         * Tel
         */
        let telfield = setComponent(boltInput, {
            type: 'tel',
            value: '',
            placeholder: 'Tel field',
            required: true,
            formSubmission: true,
            requiredAlertMessage: 'This tel field is required'
        });
        expect(telfield.vm.isRequired).toBe(true);
        expect(telfield.find('.validation_message').text()).toBe('This tel field is required');

        let telfield1 = setComponent(boltInput, {
            type: 'tel',
            value: 'abcde',
            placeholder: 'Tel field',
            required: true,
            formSubmission: true,
            requiredAlertMessage: 'This tel field is required',
            invalidAlertMessage: 'This field should be in digits'
        });
        expect(telfield1.vm.isRequired).toBe(false);
        expect(telfield1.find('.validation_message').text()).toBe('This field should be in digits');

        let telfield2 = setComponent(boltInput, {
            type: 'tel',
            value: '123467',
            placeholder: 'Tel field',
            minlength: 8,
            required: true,
            formSubmission: true
        });
        expect(telfield2.vm.isRequired).toBe(false);
        expect(telfield2.find('.validation_message').text()).toBe('Tel field must have at least 8 characters.');

        /**
         * Password type
         */
        let pwdfield = setComponent(boltInput, {
            type: 'password',
            value: '',
            placeholder: 'Pwd field',
            required: true,
            formSubmission: true,
            requiredAlertMessage: 'This tel field is required'
        });
        expect(pwdfield.vm.isRequired).toBe(true);
        expect(pwdfield.find('.validation_message').text()).toBe('This tel field is required');

        let pwdfield1 = setComponent(boltInput, {
            type: 'password',
            value: 'abc123',
            placeholder: 'Pwd field',
            required: true,
            formSubmission: true,
            minlength: 6,
            requiredAlertMessage: 'This tel field is required'
        });
        expect(pwdfield1.vm.isRequired).toBe(false);
        expect(pwdfield1.find('.validation_message').text()).toBe('Pwd field must have at least 6 characters.');
    });


    // it('Should not show required message if required is true and there is value in the field', () => {
    //     let wrapper = setComponent(boltInput, {
    //         value: 'Hello World',
    //         required: true,
    //         formSubmission: true,
    //         requiredAlertMessage: 'The field is required'
    //     });
    //     expect(wrapper.vm.requiredMessage()).toBe(false);
    // });
});